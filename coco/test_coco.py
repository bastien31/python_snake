import unittest
from .coco_gpt import repeat

class TestCoco(unittest.TestCase):

    def test_repeat(self):
        answer = repeat("Bonjour")
        self.assertEqual(answer, "Bonjour")


if __name__ == '__main__':
    unittest.main()